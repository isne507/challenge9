#include <iostream>
#include "list.h"

using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::headPush(int list)//Add element to front of list
{
	Node *tmp = new Node(list); //create new node name is tmp

	if (isEmpty()) //not have any element in list 
	{
		tail = tmp; // tail point to tmp
		head = tmp; // head point to tmp
	}
	else {//list is not empty
		tmp->next = head;//tmp point to head
		head = tmp; //now we have new head
	}

}

void List::tailPush(int list)//Add element to tail of list
{
	Node *tmp = new Node(list); //create new node name is tmp
	if (isEmpty())//not have any element in list
	{
		head = tmp; // head point to tmp
		tail = tmp; // tail point to tmp
	}
	else //list is not empty
	{
		tail->next = tmp;//tail point to next element is tmp
		tail = tmp;// now tail point to tmp
	}
}

int List::headPop()//Remove and return element from front of list
{
	int c; // create c to collect element
	Node *tmp = head; //create node name is tmp point to head
	if (isEmpty())//not have any element in list
	{
		return NULL;//not have any element to delete
	}
	else if (head == tail)//having one element in list
	{
		c = tmp->info;//c point to head
		head = NULL; // point new head to nothing
		tail = NULL; // point tail to nothing 
		delete tmp;
		return c;
	}
	else//having more than one element in list
	{
		head = head->next; //point head to next element
		c = tmp->info;//c point to element in head
		delete tmp;
		return c;
	}

}

int List::tailPop()//Remove and return element from tail of list
{
	int c;
	Node *tmp = head;
	if (isEmpty())//not have any element in list
	{
		return NULL;//not have any element to delete
	}
	else if (head->next == NULL)
	{
		c = tmp->info;
		head = NULL;
		tail = NULL;
		delete tmp;
		return c;
	}
	else
	{
		while (tmp->next != tail)//check untill tmp that point to next element is not equal to tail
		{
			tmp = tmp->next; //tmp point to next element untill find tail
		}
		tail = tmp;//tail point to tmp
		tmp = tmp->next;
		c = tmp->info;//c point to element in tmp
		delete tmp;
		tail->next = NULL;
		return c;

	}

}

void List::deleteNode(int list)//Delete a particular value
{
	if (isEmpty())//not have any element in list
	{
		return;//nothing to delete
	}
	while (head->info == list) //check element in head is equal to value(set eaual to list)
	{
		Node *d = head;
		head = head->next;
		delete d;
		if (isEmpty()) return;
	}
	Node *p = 0;//create node name is p=previous 
	Node *tmp = head;
	while (tmp != tail)
	{
		if (tmp->info != list)
		{
			p = tmp; //set p is the same order of tmp
			tmp = tmp->next; //check untill find list
		}
		else
		{
			Node *q = tmp; //and then set q is the same order of tmp that be found list
			tmp = tmp->next;//tmp point to next element
			delete q;
			p->next = tmp;
		}
	}
	if (tail->info == list)
	{
		tail = p;
		p = p->next;
		delete p;
		tail->next = NULL;
	}
}
bool List::isInList(int list) //Check if a particular value is in the list
{
	Node *tmp = head;
	if (isEmpty())
	{
		return false;
	}
	while (tmp != tail)
	{
		if (tmp->info != list)
		{
			tmp = tmp->next;
		}
		else
		{
			return true;
		}
	}
	if (tmp->info == list) return true;
	else return false;
}
void List::display()//Function display
{
	Node *tmp = head;

	while (tmp != tail)
	{
		cout << tmp->info;
		tmp = tmp->next;
	}
	if (tmp != NULL) cout << tmp->info;
	return;
}
	


